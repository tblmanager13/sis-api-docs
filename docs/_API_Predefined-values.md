# Predefined values

## Response Status Codes

This codes represent the indicator response status

Code | Name
---------|----------
 2 | Approved
 5 | In Review
 6 | Pending
 7 | Rejected
 8 | Not audited
 9 | Audited

 ## Periodicity types

This represent the type of a reference

Code | Name | Example
---------|----------|--------
 1 | Monthly | 2020-01 ... 2020-12
 2 | Bimonthly | 2020-01, 2020-03, 2020-05, 2020-07, 2020-09, 2020-11
 3 | Quartely | 2020-01, 2020-04, 2020-07, 2020-10
 4 | Semi-annually | 2020-01, 2020-07
 5 | Annualy | 2020
 6 | Harverst Monthly | 2020-01 ... 2020-12
 7 | Haverst Annually | 2020

 ## Indicator Formats

Code | Format
---------|----------
 0 | Default
 1 | Line Repeater
 2 | Recurrent