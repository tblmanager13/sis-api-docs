# Authentication

We have three ways to authenticate your user:

First is using your login and password, you can see more details in the Authentication endpoint (see the password example) in this docs.

Second is using your company client authentication, you can see more details in the Authentication endpoint  (see the client credentials example) in this docs.

If you don't have an account with a password or can't request an access token every time, you can create a Personal Access Token, you can create one in "Your profile > My account > Personal API Tokens" after you logged in the system.

**Authenticating with the Access Token**

With the Access Token, you can authenticate others requests with two methods:

* Send a header "Authorization: Bearer {access_token}"
* Or send a query param ?api_token={access_token}