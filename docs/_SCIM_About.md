# SCIM

As an application developer, you can use the System for Cross-Domain Identity Management (SCIM) user management API to enable automatic provisioning of users and groups between TBL Manager and Azure AD (AAD) or between TBL Manager and your application. This article describes how to implement a SCIM endpoint and integrate with your provisioning service. The SCIM specification provides a common user schema for provisioning. When used in conjunction with federation standards like SAML or OpenID Connect, SCIM gives administrators an end-to-end, standards-based solution for access management.

SCIM is a standardized definition of two endpoints: a /Users endpoint and a /Groups endpoint. It uses common REST verbs to create, update, and delete objects, and a pre-defined schema for common attributes like group name, username, first name, last name and email. For example, any compliant SCIM client knows how to make an HTTP POST of a JSON object to the /Users endpoint to create a new user entry. Instead of needing a slightly different API for the same basic actions, apps that conform to the SCIM standard can instantly take advantage of pre-existing clients, tools, and code.

The standard user object schema and rest APIs for management defined in SCIM 2.0 (RFC [7642](https://tools.ietf.org/html/rfc7642), [7643](https://tools.ietf.org/html/rfc7643), [7644](https://tools.ietf.org/html/rfc7644)) allow identity providers and apps to more easily integrate with each other.

There are several endpoints defined in the SCIM RFC. You can start with the /User endpoint and then expand from there.

Endpoint | Description
---------|----------
/Users | Perform CRUD operations on a user object.
/Groups | Perform CRUD operations on a group object.
/Schemas | The set of attributes supported by each client and service provider can vary. One service provider might include name, title, and emails, while another service provider uses name, title, and phoneNumbers. The schemas endpoint allows for discovery of the attributes supported.
/Bulk | Bulk operations allow you to perform operations on a large collection of resource objects in a single operation (e.g. update memberships for a large group).
/ServiceProviderConfig | Provides details about the features of the SCIM standard that are supported, for example the resources that are supported and the authentication method.

Use the general guidelines when implementing a SCIM endpoint to ensure compatibility with AAD:

**General:**
- id is a property for all resources. Every response returns a resource with a internal hashed ID used for PUT/PATCH and DELETE operations.
- It isn't necessary to include the entire resource in the PATCH response.


## User provisioning and deprovisioning

The following illustration shows an exaple set of messages that AAD sends to a SCIM service to manage the lifecycle of a user in your application's identity store.

![scim-figure-4.png](https://tbl-sis.s3.sa-east-1.amazonaws.com/docs/scim-figure-4.png)

## Group provisioning and deprovisioning

Group provisioning and deprovisioning are optional. When implemented and enabled, the following illustration shows an example of set of messages that AAD sends to a SCIM service to manage the lifecycle of a group in your application's identity store. Those messages differ from the messages about users in two ways:

![scim-figure-5.png](https://tbl-sis.s3.sa-east-1.amazonaws.com/docs/scim-figure-5.png)
