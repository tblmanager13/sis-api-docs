# SCIM - Configuration - Azure AD

## Managing user account provisioning for enterprise apps in the Azure portal

This article describes the general steps for managing automatic user account provisioning and de-provisioning for applications that support it. User account provisioning is the act of creating, updating, and/or disabling user account records in an application’s local user profile store. 

## Getting started

**Connect the TBL Manager application used for authentication:**
1. Sign in to the [AAD portal](https://aad.portal.azure.com/). Note that you can get access a free trial for Azure Active Directory with P2 licenses by signing up for the [developer program](https://developer.microsoft.com/office/dev-program)
2. Select Enterprise applications from the left pane. A list of all configured apps is shown, including apps that were added from the gallery.
3. Select the pre-configured TBL Manager application
4. In the app management screen, select Provisioning in the left panel.
5. In the Provisioning Mode menu, select Automatic. 
![scim-figure-2b.png](https://tbl-sis.s3.sa-east-1.amazonaws.com/docs/scim-figure-2b.png)

6. In the Tenant URL field, enter the URL of the application's SCIM endpoint: https://sis2.tblmanager.com/scim/v2/
7. In the Secret Token field, enter your token, if you don't have one, you need to request for the TBL Manager contact. 
8. Select Test Connection to have Azure Active Directory attempt to connect to the SCIM endpoint. If the attempt fails, error information is displayed.
9. If the attempts to connect to the application succeed, then select Save to save the admin credentials.
10. In the Mappings section, there are two selectable sets of attribute mappings: one for user objects and one for group objects. Select each one to review the attributes that are synchronized from Azure Active Directory to your app. The attributes selected as Matching properties are used to match the users and groups in your app for update operations. Select Save to commit any changes. *You can optionally disable syncing of group objects by disabling the "groups" mapping.*
11. Under Settings, the Scope field defines which users and groups are synchronized. Select Sync only assigned users and groups (recommended) to only sync users and groups assigned in the Users and groups tab.
12. Once your configuration is complete, set the Provisioning Status to On.
13. Select Save to start the Azure AD provisioning service.
14. If syncing only assigned users and groups (recommended), be sure to select the Users and groups tab and assign the users or groups you want to sync.

Once the initial cycle has started, you can select Provisioning logs in the left panel to monitor progress, which shows all actions done by the provisioning service on your app. For more information on how to read the Azure AD provisioning logs, see [Reporting on automatic user account provisioning](https://docs.microsoft.com/en-us/azure/active-directory/app-provisioning/check-status-user-account-provisioning).

*The initial cycle takes longer to perform than later syncs, which occur approximately every 40 minutes as long as the service is running.*